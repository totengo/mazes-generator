import pygame
import numpy
from random import randint

class Grafo:
    def __init__(self, n=0, connections=None):
        self.n = n
        self.adj = [[set(), {}, []] for i in range(n)]
        if connections!=None:
            for c, i in zip(connections, range(len(connections))):
                self.adj[i][0] = c
    
    def addEdge(self, node1, node2, data=[]):
        self.adj[node1][0].add(node2)
        self.adj[node2][0].add(node1)
        if len(data)!=0:
            self.adj[node1][1][node2] = data
            self.adj[node2][1][node1] = data
    
    def removeEdge(self, node1, node2):
        if node1 in self.adj[node2][0]:
            self.adj[node1][0].remove(node2)
            self.adj[node2][0].remove(node1)
            if node1 in self.adj[node2][1]:
                self.adj[node1][1].pop(node2)
                self.adj[node2][1].pop(node1)
    
    def getEdgeData(self, node1, node2):
        if node1 in self.adj[node2][0] and node1 in self.adj[node2][1]:
            return self.adj[node1][1][node2]
        else:
            return None
    
    def areConnected(self, node1, node2):
        if node1 in self.adj[node2][0]:
            return True
        else:
            return False
    
    def getNodeSet(self, node):
        return self.adj[node][0]
    
    def getAPath(self, startNode, endNode, exclusions = []):
        if self.areConnected(startNode, endNode):
            return [(startNode, endNode)]
        else:
            for nodo in self.adj[startNode][0]:
                if not nodo in exclusions:
                    potential = self.getAPath(nodo, endNode, exclusions+[startNode])
                    if not potential is None:
                        return [(startNode, nodo)]+potential
            return None
    
dimensao = 40
labirinto = set()
labirinto_grafo = Grafo(dimensao*dimensao)
paredes_sortear = []
paredes_excluidas = []
celula = randint(0,dimensao*dimensao-1) 
last_wall = None

row = lambda e1: (int)(e1/dimensao)

while len(labirinto)<dimensao*dimensao:
    #celula = dimensao-1 if len(labirinto)==0 else celula
    labirinto.add(celula)
    for parede in paredes_sortear:
        if parede[1] in labirinto:
            paredes_sortear.remove(parede)
    conditions = [row(celula+1)==row(celula), row(celula-1)==row(celula), True, True]
    candidatos = [celula+1, celula-1, celula+dimensao, celula-dimensao]
    paredes_sortear += [(celula, candidatos[i]) for i in range(len(candidatos)) 
                       if (conditions[i] and candidatos[i]>=0 and candidatos[i]<dimensao*dimensao and not candidatos[i] in labirinto)]
    if not last_wall is None:
        paredes_excluidas.append(last_wall)
        labirinto_grafo.addEdge(last_wall[0], last_wall[1])
    last_wall = paredes_sortear[randint(0, len(paredes_sortear)-1)]
    celula = last_wall[1]
    
print(paredes_excluidas)

#Com as paredes excluídas, para cada elemento basta desenhar todas as paredes não excluídas. Os contornos externos sempre são desenhados

DISPLAY_SIZE = 800
pygame.display.set_caption("labirinto gerado")
tela = pygame.display.set_mode((DISPLAY_SIZE, DISPLAY_SIZE))
tela.fill((0, 0, 0))

#Coloco 10% de margens e o espaço restante para desenhar o labirinto
total_draw_size = 0.9*DISPLAY_SIZE
margin_size = (DISPLAY_SIZE-total_draw_size)/2.0
#Ignoro a espessura das linhas
block_size = total_draw_size/dimensao
corners = [(margin_size, margin_size), (DISPLAY_SIZE-margin_size, margin_size),
           (DISPLAY_SIZE-margin_size, DISPLAY_SIZE-margin_size), (margin_size, DISPLAY_SIZE-margin_size)]

block_center = lambda i: (block_size/2+margin_size+(i%dimensao)*block_size, block_size/2+margin_size+(int(i/dimensao))*block_size)

for i in range(len(corners)):
    pygame.draw.line(tela, (255, 255, 255), corners[i], corners[(i+1)%len(corners)], 5)

for i in range(dimensao*dimensao):
    row = (int)(i/dimensao)
    col = i%dimensao
    if col%dimensao!=(dimensao-1) and (not (i, i+1) in paredes_excluidas) and (not (i+1, i) in paredes_excluidas):
        pygame.draw.line(tela, (255, 255, 255), (margin_size+(col+1)*block_size, margin_size+row*block_size), 
                         (margin_size+(col+1)*block_size, margin_size+(row+1)*block_size), 5)
    if row!=(dimensao-1) and (not (i, i+dimensao) in paredes_excluidas) and (not (i+dimensao, i) in paredes_excluidas):
        pygame.draw.line(tela, (255, 255, 255), (margin_size+col*block_size, margin_size+(row+1)*block_size),
                         (margin_size+(col+1)*block_size, margin_size+(row+1)*block_size), 5)
        
pygame.display.update()

nodo1 = int(input('nó:\n'))
nodo2 = int(input('nó:\n'))

solution = labirinto_grafo.getAPath(nodo1, nodo2)
if solution is None:
    print('sem solution')
else:
    for tup in solution:
        print(block_center(tup[0]))
        print(block_center(tup[1]))
        pygame.draw.line(tela, (255, 0, 0), block_center(tup[0]), block_center(tup[1]), 5)

pygame.display.update()

clock = pygame.time.Clock()

i = 0

while True:
    clock.tick(60)
    
    i = (i+1)%10000
    for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    exit()

